module.exports = ({ name }) => {
    return `'use strict'
const Controller = require('../base.js');
const SELECT = require('../../../../config/select');
class BannerController extends Controller {
    // 列表查询
    async find() {
        const {ctx} = this;
        const opt = {page, state, sort, limit} = ctx.query;
        const select = SELECT.${name.toLocaleUpperCase()};
        const res = await ctx.service.${name}.find(opt, select);
        ctx.success(res)
    }
}
    `
}