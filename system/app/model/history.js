'use strict';

module.exports = (app) => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const HistorySchema = new Schema({
        method: {
            type: String,
            require: true,
            default: ''
        },
        ip: {
            type: String,
            default: ''
        },
        apiName: {
            type: String,
            default: ''
        },
        url: {
            type: String,
            default: ''
        },
        body: {},
        auth: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        // 接口访问状态  0 失败  1 成功  3 未知
        state: {
            type: String,
            default: '3'
        },
        // 请求状态码 
        apiStatus: {
            type: String
        },
        startAt: {
            type: Date,
            default: Date.now
        },
        endAt: {
            type: Date,
            default: Date.now
        },
        duration: {
            type: Number
        }
    });
    HistorySchema.pre('save', function (next) {
        this.duration = this.endAt - this.startAt;
        next()
    });
    return mongoose.model('history', HistorySchema);
};
