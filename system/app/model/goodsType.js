'use strict';

module.exports = app => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const GoodsTypeSchema = new Schema({
        imgs: {
            type: Array,
            require: true
        },
        name: {
            type: String,
            require: true
        },
        auth: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        createAt: {
            type: Date,
            default: Date.now
        },
        updateAt: {
            type: Date,
            default: Date.now
        }
    })

    return mongoose.model('goodsType', GoodsTypeSchema);
}