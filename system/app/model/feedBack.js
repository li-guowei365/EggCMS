'use strict';

module.exports = (app) => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const FeedBackSchema = new Schema({
        title: {
            type: String
        },
        content: {
            type: String,
            require: true
        },
        auth: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        // 0 未解决  1 已解决 
        type:{
            type: String,
            default: '0',
        },
        email: {
            type: String
        },
        phone: {
            type: String,
            require: true
        },
        weixin: {
            type: String,
            require: true
        },
        qq: {
            type: String,
            require: true
        },
        createAt: {
            type: Date,
            default: Date.now
        },
        updateAt: {
            type: Date,
            default: Date.now
        }
    });

    return mongoose.model('feedBack', FeedBackSchema);
};
