'use strict';

module.exports = (app) => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const RouterSchema = new Schema({
        methods: String,
        name: {
            type: String,
            index: 1
        },
        path: String,
        ids: String,
        reqNumber: {
            type: Number,
            default: 0
        },
        createAt: {
            type: Date,
            default: Date.now
        }
    });
    RouterSchema.pre('save', function (next) {
        this.ids = `${this.methods}_${this.path.substring(5)}`;
        next();
    });
    return mongoose.model('router', RouterSchema);
};
