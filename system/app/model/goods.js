'use strict';

module.exports = app => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const GoodsSchema = new Schema({
        imgs: {
            type: Array,
            require: true
        },
        name: {
            type: String,
            require: true
        },
        money: {
            type: Number,
            require: true
        },
        beforeMoney: {
            type: Number
        },
        detail: {
            type: String
        },
        recommend: {
            type: Boolean,
            default: false
        },
        // 商品状态 1 正常 0 删除 2下架
        type: {
            type: String,
            default: '1'
        },
        auth: {
            type: Schema.Types.ObjectId,
            ref: 'goodsType'
        },
        createAt: {
            type: Date,
            default: Date.now
        },
        updateAt: {
            type: Date,
            default: Date.now
        }
    })

    return mongoose.model('goods', GoodsSchema);
}