'use strict';

module.exports = (app) => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const CommentChildrenSchema = new Schema({
        articleId: {
            type: Schema.Types.ObjectId,
            ref: 'article'
        },
        commentId:{
            type: Schema.Types.ObjectId,
            ref: 'comment'
        },
        content: {
            type: String,
            require: true
        },
        auth: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        likesUser: [
            {
                type: Schema.Types.ObjectId,
                ref: 'user'
            }
        ],
        numbers: {
            like: {
                type: Number,
                default: 0
            }
        },
        delete: {
            type: Boolean,
            default: false
        },
        createAt: {
            type: Date,
            default: Date.now
        },
        updateAt: {
            type: Date,
            default: Date.now
        }
    });
    
    return mongoose.model('commentChildren', CommentChildrenSchema);
};
