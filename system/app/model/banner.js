'use strict';

module.exports = (app) => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const BannerSchema = new Schema({
        title: {
            type: String,
            require: true,
            default: ''
        },
        desc: {
            type: String,
            default: ''
        },
        // 广告类型  0 首页轮播   1 新闻列表  2 全局广告
        type: {
            type: String,
            default: '0'
        },
        // 状态 0 下架 1 正常
        state: {
            type: String,
            default: '0'
        },
        href: {
            type: String,
            default: ''
        },
        img: {
            type: String,
            default: ''
        },
        auth: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        createAt: {
            type: Date,
            default: Date.now
        },
        updateAt: {
            type: Date,
            default: Date.now
        }
    });

    return mongoose.model('banner', BannerSchema);
};
