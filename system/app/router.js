'use strict';

/**
 * @param {Egg.Application} app - egg application
 */

module.exports = (app) => {
	const { router, controller } = app;
	const isSignIn = app.middleware.isSignIn({}, app);
	router.get('/', controller.home.index);

	//用户
	router.get('用户列表', '/api/v1/user', isSignIn, controller.api.v1.user.find);
	router.get('超级管理员信息', '/api/v1/admininfo', controller.api.v1.user.adminInfo);
	router.get('当前登陆用户信息', '/api/v1/userinfo', isSignIn, controller.api.v1.user.userInfo);
	router.get('根据用户ID获取用户信息', '/api/v1/userinfoById', controller.api.v1.user.userInfoById);
	router.post('新增用户', '/api/v1/user', isSignIn, controller.api.v1.user.create);
	router.post('用户注册', '/api/v1/register', controller.api.v1.user.register);
	router.get('用户访问链接注册', '/api/v1/emailUrlRegister', controller.api.v1.user.emailUrlRegister);
	router.put('修改用户', '/api/v1/user', isSignIn, controller.api.v1.user.update);
	router.put('封禁，解封用户', '/api/v1/userStatus', isSignIn, controller.api.v1.user.userStatus);
	router.put('重置密码', '/api/v1/userRestPassword', isSignIn, controller.api.v1.user.restPassword);
	router.delete('删除用户', '/api/v1/user', isSignIn, controller.api.v1.user.destroy);

	//角色
	router.get('角色列表', '/api/v1/role', isSignIn, controller.api.v1.role.find);
	router.post('新增角色', '/api/v1/role', isSignIn, controller.api.v1.role.create);
	router.put('修改角色', '/api/v1/role', isSignIn, controller.api.v1.role.update);
	router.delete('删除角色', '/api/v1/role', isSignIn, controller.api.v1.role.destroy);

	// 为角色增添路由
	router.post('为角色增加权限', '/api/v1/roleAddRouter', isSignIn, controller.api.v1.role.createRouter);
	router.post('为角色移除权限', '/api/v1/roleDestroyRouter', isSignIn, controller.api.v1.role.destroyRouter);

	//路由
	router.get('路由列表', '/api/v1/router', isSignIn, controller.api.v1.router.find);

	// 用户与角色
	router.post('赋予用户角色', '/api/v1/userAddRole', isSignIn, controller.api.v1.user.addRole);
	router.post('移除用户角色', '/api/v1/destroyUserRole', isSignIn, controller.api.v1.user.destroyRole);

	// 登录登出
	router.post('登录', '/api/v1/signIn', controller.api.v1.sign.index);
	router.post('退出登录', '/api/v1/signOut', isSignIn, controller.api.v1.sign.out);

	// 消息
	router.get('消息列表', '/api/v1/message', isSignIn, controller.api.v1.message.find);
	router.get('消息详情', '/api/v1/messageDetail', isSignIn, controller.api.v1.message.detail);
	router.delete('删除消息', '/api/v1/message', isSignIn, controller.api.v1.message.destroy);
	router.put('设置消息为已读', '/api/v1/messageRead', isSignIn, controller.api.v1.message.read);

	// 文章
	router.get('文章列表', '/api/v1/article', controller.api.v1.article.find);
	router.get('文章详情', '/api/v1/articleDetail', controller.api.v1.article.dateail);
	router.post('新增文章', '/api/v1/article', isSignIn, controller.api.v1.article.create);
	router.put('修改文章', '/api/v1/article', isSignIn, controller.api.v1.article.update);
	router.put('文章置顶与取消置顶', '/api/v1/articleTopToggle', isSignIn, controller.api.v1.article.toggleTop);
	router.put('文章状态变更', '/api/v1/articleUpdateState', isSignIn, controller.api.v1.article.updateState);
	router.post('收藏或取消收藏文章', '/api/v1/likeArticle', isSignIn, controller.api.v1.article.likeArticle);
	router.delete('删除文章', '/api/v1/article', isSignIn, controller.api.v1.article.destroy);

	// 公告
	router.get('公告列表', '/api/v1/notice', controller.api.v1.notice.find);
	router.post('新增公告', '/api/v1/notice', isSignIn, controller.api.v1.notice.create);
	router.put('修改公告', '/api/v1/notice', isSignIn, controller.api.v1.notice.update);
	router.delete('删除公告', '/api/v1/notice', isSignIn, controller.api.v1.notice.destroy);

	// 评论
	router.get('评论列表', '/api/v1/comment', controller.api.v1.comment.find);
	router.post('新增评论', '/api/v1/comment', isSignIn, controller.api.v1.comment.create);
	router.post('点赞或取消点赞评论', '/api/v1/likeComment', isSignIn, controller.api.v1.comment.likeComment);
	router.delete('删除评论', '/api/v1/comment', isSignIn, controller.api.v1.comment.destroy);

	// 子评论
	router.get('子评论列表', '/api/v1/commentChildren', controller.api.v1.commentChildren.find);
	router.post('新增子评论', '/api/v1/commentChildren', isSignIn, controller.api.v1.commentChildren.create);
	router.post('点赞或取消点赞子评论', '/api/v1/likecommentChildren', isSignIn, controller.api.v1.commentChildren.likeComment);
	router.delete('删除子评论', '/api/v1/commentChildren', isSignIn, controller.api.v1.commentChildren.destroy);

	// 文章栏目
	router.get('文章栏目列表', '/api/v1/tag', controller.api.v1.tag.find);
	router.post('新增栏目', '/api/v1/tag', isSignIn, controller.api.v1.tag.create);
	router.put('修改栏目', '/api/v1/tag', isSignIn, controller.api.v1.tag.update);
	router.delete('删除栏目', '/api/v1/tag', isSignIn, controller.api.v1.tag.destroy);

	// 广告
	router.get('广告列表', '/api/v1/banner', controller.api.v1.banner.find);
	router.post('新增广告', '/api/v1/banner', isSignIn, controller.api.v1.banner.create);
	router.put('广告状态变更', '/api/v1/bannerUpdateState', isSignIn, controller.api.v1.banner.updateState);
	router.put('修改广告', '/api/v1/banner', isSignIn, controller.api.v1.banner.update);
	router.delete('删除广告', '/api/v1/banner', isSignIn, controller.api.v1.banner.destroy);

	// 文件上传
	router.post('文件上传至本地', '/api/v1/fileUpload', isSignIn, controller.api.v1.file.index);
	router.post('文件上传至七牛', '/api/v1/fileUploadQiNiu', isSignIn, controller.api.v1.fileQiniu.index);
	// router.post('头像上传至阿里云', '/api/v1/fileUploadOss', isSignIn, controller.api.v1.fileOss.head);

	// 访问记录
	router.get('访问记录', '/api/v1/history', isSignIn, controller.api.v1.history.find);
	// 数据统计
	router.get('数据统计', '/api/v1/dataCount', isSignIn, controller.api.v1.dataCount.index);
};