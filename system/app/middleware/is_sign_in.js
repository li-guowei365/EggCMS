'use strict';

module.exports = (option, app) => {
    return async (ctx, next) => {
        const userSign = await app.userSign(ctx);
        // 未登录
        if (userSign.error) {
            ctx.throw(userSign.status, userSign.message,{
                code:'sign_not'
            });
        }
        // 已登陆
        await next();
    }
};
