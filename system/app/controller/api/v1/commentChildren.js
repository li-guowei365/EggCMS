'use strict';

const Controller = require('../base.js');
const SELECT = require('../../../../config/select');
class CommentChildrenController extends Controller {
	/**
     *子评论获取
     */
	async find() {
		const { ctx } = this;
		const { commentId, page, limit } = ctx.query;

		const Rules = {
			commentId: {
				type: 'string',
				keyname: '父评论ID'
			}
		};
		ctx.proving(Rules, ctx.query);

		const select = SELECT.COMMENT_CHILDREN;
		const res = await ctx.service.commentChildren.find({ commentId, page, limit }, select);
		ctx.success(res);
	}

	/**
     * 新增子评论
     */
	async create() {
		const { ctx, app } = this;
		const body = ctx.request.body;
		const articleId = body.articleId;
		const commentId = body.commentId;
		let content = body.content;

		const Rules = {
			articleId: {
				type: 'objectId',
				keyname: '文章ID'
			},
			commentId: {
				type: 'objectId',
				keyname: '父评论ID'
			},
			content: {
				type: 'string',
				keyname: '评论内容'
			}
		};
		ctx.proving(Rules);
		// 取出被@的用户
		let sendees = app.fetchUsers(content);
		// 将文章内容中被@的用户进行格式化
		content = app.linkUsers(content);
		// 写入数据库
		const comment = await ctx.service.commentChildren.create({
			articleId,
			commentId,
			content
		});

		// 通知被@的用户
		ctx.service.article.find({ id: articleId }).then(async (res) => {
			console.log('评论时获取到的文章信息：', res);
			// 给被@的用户发消息
			this.submitMsg({
				sendees,
				// 用户 xxx 在文章 xxx 的评论中提到了你
				content: [
					{
						type: 'string',
						value: '用户',
					},
					{
						type: 'user',
						relationId: ctx.state.user._id,
						value: ctx.state.user.name,
					},
					{
						type: 'string',
						value: '在文章',
					},
					{
						type: 'article',
						relationId: articleId,
						value: res.title,
					},
					{
						type: 'string',
						value: '的',
					},
					{
						type: 'commentChildren',
						relationId: commentId,
						value: '评论',
					},
					{
						type: 'string',
						value: '中提到了你',
					},
				],
				type: 0
			});
			if (ctx.state.user._id.toString() == res.auth._id.toString()) {
				this.submitMsg({
					sendees: [comment.auth],
					// 用户 xxx 评论了您在文章 xxx 中的评论
					content: [
						{
							type: 'string',
							value: '文章',
						},
						{
							type: 'article',
							relationId: articleId,
							value: res.title,
						},
						{
							type: 'string',
							value: '的作者',
						},
						{
							type: 'user',
							relationId: ctx.state.user._id,
							value: ctx.state.user.name,
						},
						{
							type: 'string',
							value: '回复了你在文章中的',
						},
						{
							type: 'commentChildren',
							relationId: commentId,
							value: '评论',
						},
					],
					type: 0
				});
			} else {
				// 给评论作者发消息
				this.submitMsg({
					sendees: [comment.auth],
					// 用户 xxx 评论了您在文章 xxx 中的评论
					content: [
						{
							type: 'string',
							value: '用户',
						},
						{
							type: 'user',
							relationId: ctx.state.user._id,
							value: ctx.state.user.name,
						},
						{
							type: 'string',
							value: '评论了您在文章',
						},
						{
							type: 'article',
							relationId: articleId,
							value: res.title,
						},
						{
							type: 'string',
							value: '中的',
						},
						{
							type: 'commentChildren',
							relationId: commentId,
							value: '评论',
						},
					],
					type: 0
				});
			}
		});
		ctx.success({ id: comment.id });
	}

	/*
     * 删除评论
     * */
	async destroy() {
		const { ctx } = this;
		const body = ctx.request.body;
		const id = body.id;

		const Rules = {
			id: {
				type: 'objectId',
				keyname: '子评论ID'
			}
		};
		ctx.proving(Rules);

		const article = await ctx.service.commentChildren.destroy(id);

		ctx.success({
			message: '删除成功'
		});
	}

	/*
     * 点赞，取消点赞
     * */
	async likeComment() {
		const { ctx } = this;
		const body = ctx.request.body;
		const id = body.id;
		const Rules = {
			id: {
				type: 'objectId',
				keyname: '子评论ID'
			}
		};
		ctx.proving(Rules);
		const res = await ctx.service.commentChildren.userLikeComment(id);
		ctx.success(res);
	}
}

module.exports = CommentChildrenController;
