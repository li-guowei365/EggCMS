'use strict';

const Controller = require('../base.js');
const SELECT = require('../../../../config/select');
class MessageController extends Controller {
	/**
     * 消息列表
     */
	async find() {
		const { ctx } = this;
		const { page, limit } = ctx.query;
		ctx.proving(Rules, ctx.query);
		const select = SELECT.MESSAGE;
		const res = await ctx.service.message.findMsg({ page, limit, select });
		ctx.success(res);
	}

	/**
	 * 消息详情
	 */
	async detail() {
		const { ctx } = this;
		const { id } = ctx.query;

		const Rules = {
			id: {
				type: 'objectId'
			}
		};
		ctx.proving(Rules, ctx.query);
		const select = SELECT.MESSAGE;
		const res = await ctx.service.message.findMsg({ id, select });
		ctx.success(res);
	}

	/**
     *消息删除
     */
	async destroy() {
		const { ctx } = this;
		const body = ctx.request.body;
		let id = body.id;
		if (typeof id === 'string') {
			id = id.split(',');
		}
		if (id.length === 1) {
			id = id[0];
		}
		const res = await ctx.service.message.destroy(id);
		ctx.success({
			message: '删除成功'
		});
	}
	/**
     *消息设置为已读/全部设置为已读
     */
	async read() {
		const { ctx } = this;
		const body = ctx.request.body;
		let id = body.id;
		let isall = body.isall;

		if (isall) {
			await ctx.service.message.readAll();
		} else {
			const Rules = {
				id: {
					type: 'string',
					keyname: '消息id'
				}
			};
			ctx.proving(Rules);
			await ctx.service.message.read(id);
		}
		ctx.success({
			message: '设置成功'
		});
	}
}
module.exports = MessageController;
