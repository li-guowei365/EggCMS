'use strict';

const Controller = require('../base.js');
const SELECT = require('../../../../config/select');

class ArticleController extends Controller {
	/**
     *文章查询
     */
    async find() {
        const { ctx, app } = this;
        const { id, title, tag, state, top, page, limit, sort } = ctx.query;
        
        const Rules = {
            id: {
                type: 'objectId',
                required: false
            }
        };
        ctx.proving(Rules, ctx.query);
        let tagarr;
        if (tag) {
            tagarr = JSON.parse(tag);
        }
        const select = SELECT.ARTICLE;
        const res = await ctx.service.article.find(
            {
                id,
                title,
                tag: tagarr,
                page,
                limit,
                sort,
                state,
                top
            },
            select
        );
        ctx.success(res);
    }

	/**
     *文章详情获取
     */
    async dateail() {
        const { ctx, app } = this;
        const { id } = ctx.query;
        const { token } = ctx.header;
        const Rules = {
            id: {
                type: 'objectId',
                keyname: '文章ID'
            }
        };
        ctx.proving(Rules, ctx.query);
        // 进行一次登陆验证，判断是否被收藏
        const user = await app.userSign(ctx);

        const select = SELECT.ARTICLE_DETAIL;
        const res = await ctx.service.article.find(
            {
                id
            },
            select
        );
        ctx.success(res);
    }

	/**
     * 新增文章
     */
    async create() {
        const { ctx, app } = this;
        const body = ctx.request.body;
        const title = body.title;
        let content = body.content;
        const tags = body.tags;
        const desc = body.desc;
        const top = body.top;

        const Rules = {
            title: {
                type: 'string',
                keyname: '文章标题'
            },
            content: {
                type: 'string',
                keyname: '文章内容'
            }
        };
        ctx.proving(Rules);
        // 角色权限认证，只有超级管理员
        await ctx.powerValidate();

        const article = await ctx.service.article.create({
            title,
            content,
            tags,
            desc,
            top
        });
        ctx.success({
            id: article.id
        });
    }

	/*
     * 修改文章
     * */
    async update() {
        const { ctx } = this;
        const body = ctx.request.body;
        const title = body.title;
        const content = body.content;
        const desc = body.desc;
        const tag = body.tag || [];
        const top = body.top;
        const id = body.id;

        const Rules = {
            id: {
                type: 'objectId'
            },
            title: {
                type: 'string',
                keyname: '文章标题'
            },
            content: {
                type: 'string',
                keyname: '文章内容'
            }
        };
        ctx.proving(Rules);

        const article = await ctx.service.article.update(id, {
            title,
            content,
            tag,
            desc,
            top
        });
        console.log(article);
        ctx.success({
            message: '修改成功',
            id: article.id,
            updateAt: this.app.dateFormat(article.updateAt)
        });
    }

	/*
     * 置顶/取消置顶文章
     * */
    async toggleTop() {
        const { ctx } = this;
        const body = ctx.request.body;
        const id = body.id;

        const Rules = {
            id: {
                type: 'objectId'
            }
        };
        ctx.proving(Rules);
        const select = SELECT.ARTICLE_DETAIL;
        const art = await ctx.service.article.find(
            {
                id
            },
            select
        );
        const top = art.top
        let text = top ? '取消置顶' : '置顶'
        const article = await ctx.service.article.update(id, {
            top: !top
        });
        ctx.success({
            message: text + '成功',
            id: article.id
        });
    }

	/**
	 * 更改文章状态
	 *
	 * @param {String} id @default null -文章id
	 * @param {String} state @default null -变更后状态 1=变更为上架 2=变更为下架
	 *
     **/
    async updateState() {
        const { ctx } = this;
        const body = ctx.request.body;
        const id = body.id;
        const state = body.state;

        const Rules = {
            id: {
                type: 'objectId',
                keyname: '文章ID'
            },
            state: {
                type: 'string',
                keyname: '变更状态'
            },
        };
        ctx.proving(Rules);
        const article = await ctx.service.article.update(id, {
            state
        });
        ctx.success({
            message: '文章状态变更成功',
            id: article.id,
            state: article.state
        });
    }
	/*
     * 删除文章
     * */
    async destroy() {
        const { ctx } = this;
        const id = ctx.query.id;

        const Rules = {
            id: {
                type: 'objectId',
                keyname: '文章ID'
            }
        };
        ctx.proving(Rules, ctx.query);
        // 角色权限认证
        const oldArticle = await ctx.service.article.find({ id });
        if (oldArticle) {
            await ctx.powerValidate(oldArticle.auth._id);
        }
        const article = await ctx.service.article.update(id, {
            delete: true
        });

        ctx.success({
            message: '删除成功'
        });
    }

	/*
     * 收藏文章
     * */
    async likeArticle() {
        const { ctx } = this;
        const body = ctx.request.body;
        const articleId = body.articleId;
        const Rules = {
            articleId: {
                type: 'objectId',
                keyname: '文章ID'
            }
        };
        ctx.proving(Rules);
        const res = await ctx.service.article.userLikeArticle(articleId);
        ctx.success(res);
    }

	/**
     *文章标签查询
     */
    async findTag() {
        const { ctx } = this;
        const tags = await ctx.service.article.findTag();
        ctx.success(tags);
    }
}

module.exports = ArticleController;
