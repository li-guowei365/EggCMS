'use strict';

const Controller = require('../base.js');

class HistoryController extends Controller {
	/**
     * 历史记录查询
     */
	async find() {
		const { ctx, app } = this;
		const { method, ip, apiName, url, auth, state, apiStatus, sort, startAt, endAt, page, durationStart, durationEnd, durationSort, limit } = ctx.query;
		let dStart = Number(durationStart),
			dEnd = Number(durationEnd)

		if (durationStart && !durationEnd) {
			dEnd = Number(durationStart)
		}
		if (!durationStart && durationEnd) {
			console.log(Number(durationEnd))
			dStart = Number(durationEnd)
		}
		if (durationStart && durationEnd) {
			dStart = Number(durationStart) || null
			dEnd = Number(durationEnd) || null
		}

		const res = await ctx.service.history.find(
			{
				method,
				ip,
				apiName,
				url,
				auth,
				state,
				apiStatus,
				sort,
				startAt,
				durationStart: dStart,
				durationEnd: dEnd,
				durationSort,
				endAt,
				page,
				limit
			}
		);
		ctx.success(res);
	}
}

module.exports = HistoryController;
