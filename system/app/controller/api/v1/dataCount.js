'use strict'
const Controller = require('../base.js')
const dayjs = require('dayjs')

class DataCountController extends Controller {
    async index() {
        const {ctx} = this
        const date = dayjs()
        const start = `${date.$y}-${date.$M + 1}-${date.$D} 00:00:00`
        const articleCount = ctx.model.Article.find({delete: false}).count()
        const userCount = ctx.model.User.find().count()
        const apiCount = ctx.model.History.find({startAt: {$gte: new Date(start)}}).count()
        try {
            const res = await Promise.all([articleCount, userCount, apiCount])
            ctx.success({
                article: res[0],
                user: res[1],
                api: res[2],
            })
        } catch (e) {
            ctx.throw(500, '数据统计错误', {
                code: 'data_count_error',
                errors: e
            })
        }

    }
}

module.exports = DataCountController
