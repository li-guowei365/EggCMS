'use strict';

const Controller = require('egg').Controller;
const bcrypt = require('bcryptjs');

class UserController extends Controller {
	/**
     * 用户登录
     */
    async index() {
        const { ctx } = this;
        const body = ctx.request.body;
        const { username, password, loginType } = body; // loginType: 登陆类型 默认不传，1为登陆后端
        // 获取用户
        const { list } = await ctx.service.user.find({ username });
        const user = list[0]

        if (!user) {
            ctx.throw(404, '用户不存在', {
                code: 'user_not'
            });
            return;
        }
        if (user.delete) {
            ctx.throw(404, '用户已被封禁', {
                code: 'user_close'
            });
        }
        if (!user.root && loginType === '1') {
            console.log(user)
            if (!user.loginAdmin) {
                ctx.throw(402, '您无权登陆后端管理系统', {
                    code: 'user_auth_not_sign'
                });
            }
        }
        console.log(user)
        // 验证密码
        const pwdMatch = await bcrypt.compare(password, user.password);
        if (!pwdMatch) {
            ctx.throw(402, '密码错误', {
                code: 'password_error'
            });
            return;
        }
        console.log('验证密码')
        console.log(pwdMatch)
        // 验证token
        let token;
        // 过期时间
        const expiresIn = '100h';
        if (user.token) {
            // token已存在并且尚未过期
            try {
                const tokenMatch = await ctx.app.jwt.verify(user.token, ctx.app.config.jwt.secret);
                token = user.token;
                console.log('使用现有token');
            } catch (e) {
                // 生成token
                token = await ctx.app.jwt.sign(
                    {
                        id: user.id
                    },
                    ctx.app.config.jwt.secret,
                    { expiresIn }
                );
                console.log('现有token失效，生成token');
            }
        } else {
            // 生成token
            console.log('没有token，生成token');
            token = await ctx.app.jwt.sign(
                {
                    id: user.id
                },
                ctx.app.config.jwt.secret,
                { expiresIn }
            );
        }

        // 储存token
        try {
            await ctx.service.user.updateToken(user.id, token);
            ctx.success({
                token
            });
        } catch (e) {
            ctx.throw(500, '登录失败，请重试', {
                code: 'sign_error'
            });
        }
    }

	/**
     * 退出登录
     */
    async out() {
        const { ctx } = this;
        const token = ctx.request.header.token;
        // 解码token，获取id
        const { id } = await ctx.app.jwt.decode(token);
        console.log(id);
        // 删除储存的token
        await ctx.service.user.updateToken(id, null);
        ctx.success({
            message: '退出成功'
        });
    }
}

module.exports = UserController;
