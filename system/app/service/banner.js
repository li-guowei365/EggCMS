'use strict';

const Service = require('egg').Service;

class BannerService extends Service {
    /**
     * 查询列表
     *
     * @param {String} title @default null -名称
     * @param {Boolean} sort @default -1 -排序
     * @param {Number} page @default 1 -当前页
     * @param {Number} state @default null -状态
     * @param {Number} limit @default 10 -返回条数
     * @param {String} select @default null -要显示的key
     */
    async find({ title = '', state = null, sort, page = 1, type, limit = 10 } = {}, select = null) {
        const { ctx } = this;
        const userpopulate = 'headImg role username name';
        const query = {}
        type ? query.type = type : true
        state ? query.state = state : true

        let sorts = {
            createAt: -1
        }
        if (Array.isArray(sort) && sort.length > 0) {
            sorts = {}
            for (let i of sort) {
                sorts[i.key] = i.value
            }
        }

        const res = await ctx.model.Banner
            .find(query)
            .regex('title', new RegExp(title, 'i'))
            .sort(sorts)
            .skip((Number(page) - 1) * Number(limit))
            .limit(Number(limit))
            .populate('auth', userpopulate)
            .select(select);
        const count = await ctx.model.Banner.find().regex('title', new RegExp(title, 'i')).count();
        return {
            count,
            page,
            list: res
        };
    }

    /**
     * 新增
     *
     * @param {String} title - 标题
     * @param {String} desc - 内容
     * @param {String} type - 类型 默认0
     * @param {String} state - 状态 默认下架
     * @param {String} href - 链接
     * @param {String} img - 图片
     */
    async create({ title, desc, type, state = 0, href, img } = {}) {
        const { ctx } = this;
        return await ctx.model.Banner.create({
            title,
            type,
            state,
            desc,
            href,
            img,
            auth: ctx.state.user._id
        });
    }

    /**
     * 修改
     *
     * @param {String} id -id
     * @param {Object} payload -修改条件
     */
    async update(id, payload = {}) {
        const { ctx } = this;
        const banner = await ctx.model.Banner.findById(id);
        if (!banner || banner.delete) {
            ctx.throw(404, '广告不存在或已被删除', {
                code: 'banner_not'
            });
        }
        // 权限验证
        await ctx.powerValidate(banner.auth._id);

        payload.updateAt = Date.now();
        return await ctx.model.Banner.findByIdAndUpdate(id, payload);
    }

    /**
     * 删除
     *
     * @param {String} id -id
     */
    async destroy(id) {
        const { ctx } = this;
        // 权限验证
        await ctx.powerValidate();
        return await ctx.model.Banner.findOneAndRemove(id);
    }
}

module.exports = BannerService;
