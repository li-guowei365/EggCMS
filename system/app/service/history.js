'use strict';

const Service = require('egg').Service

class HistoryService extends Service {
    /**
     * 查询记录
     *
     * @param {String} method - 请求方式
     * @param {String} ip - 来源ip
     * @param {String} apiName - 接口中文名称
     * @param {String} url - 请求接口地址
     * @param {String} auth - 请求用户
     * @param {Number} durationStart - 请求时长范围
     * @param {Number} durationEnd - 请求时长范围
     * @param {Number} durationSort - 请求时长排序
     * @param {String} state - 请求状态
     * @param {String} apiStatus - 请求状态码
     * @param {String} startAt - 开始请求时间范围
     * @param {String} sort @default -1 - 按时间排序默认倒序
     * @param {String} endAt - 结束请求时间范围
     * @param {Number} page @default 1 -当前页
     * @param {Number} limit @default 10 -返回条数
     */
    async find({
        method,
        apiName = '',
        startAt,
        endAt,
        apiStatus,
        auth,
        url = '',
        durationStart,
        durationEnd,
        durationSort,
        state = null,
        ip = '',
        sort = -1,
        page = 1,
        limit = 10
    }) {
        const { ctx } = this;
        const userpopulate = 'name';
        const query = {}
        state ? query.state = state : true
        method ? query.method = method : true
        apiStatus ? query.apiStatus = apiStatus : true
        auth ? query.auth = auth : true
        startAt ? query.startAt = { $gte: new Date(startAt), $lte: new Date(endAt) } : true
        durationStart ? query.duration = { $gte: durationStart, $lte: durationEnd } : true

        let sorts = {}

        if (sort) {
            sorts = {
                startAt: sort
            }
        }
        if (durationSort) {
            sorts = {
                duration: durationSort
            }
        }
        console.log(sorts)
        const res = await ctx.model.History
            .find(query)
            .regex('apiName', new RegExp(apiName, 'i'))
            .sort(sorts)
            .skip((Number(page) - 1) * Number(limit))
            .limit(Number(limit))
            .populate('auth', userpopulate);
        const count = await ctx.model.History.find(query).regex('apiName', new RegExp(apiName, 'i')).count();
        return {
            count,
            page,
            list: res
        };
    }

    /**
     * 新增
     *
     * @param {String} method - 请求方式
     * @param {String} ip - 来源ip
     * @param {String} apiName - 接口中文名称
     * @param {String} url - 请求接口地址
     * @param {String} body - 返回数据
     * @param {String} cookie - 携带的cookie
     * @param {String} auth - 请求用户
     * @param {String} state - 请求状态
     * @param {String} apiStatus - 请求状态码
     * @param {String} startAt - 开始请求时间
     * @param {String} endAt - 请求完成时间
     */
    async create({ method, body, ip, apiName, url, state, apiStatus, startAt, endAt, auth } = {}) {
        const { ctx } = this;

        return await ctx.model.History.create({
            method,
            ip,
            apiName,
            url,
            body,
            state,
            apiStatus,
            startAt,
            endAt,
            auth
        });
    }
}

module.exports = HistoryService;
