'use strict'

const Service = require('egg').Service

class TagService extends Service {
    /**
     * 标签列表
     *
     * @param {String} id @default null -标签id 传入此值后传入的其他值将失效
     * @param {String} name @default null -标签名称
     * @param {Number} page @default 1 -当前页
     * @param {Number} limit @default 50 -返回条数
     * @param {String} select @default null -要显示的key
     */
    async find({id, name = '', page = 1, limit = 200} = {}, select = null) {
        const {ctx, app} = this
        if (id) {
            return await ctx.model.Tag
                .findById(id)
                .select(select)
        } else {
            console.log(app)
            const res = await ctx.model.Tag
                .find()
                .regex('name', new RegExp(name, 'i'))
                .sort({createAt: -1})
                .skip((Number(page) - 1) * Number(limit))
                .limit(Number(limit))
                .select(select)
            const count = await ctx.model.Tag
                .find()
                .regex('name', new RegExp(name, 'i'))
                .count()
            return {
                count,
                page,
                list: res,
                default: app.custom.defaultTag
            }
        }
    }

    /**
     * 按名称查找标签
     *
     * @param {String} name @default null -标签名称
     */
    async findName(name, select = null) {
        const {ctx} = this
        const res = await ctx.model.Tag
            .findOne({name})
            .select(select)

        return res
    }

    /**
     * 新增标签
     *
     * @param {String} name -标签名
     */
    async create(name, desc) {
        const {ctx} = this
        const tag = await ctx.model.Tag.findOne({name})
        if (tag) {
            ctx.throw(402, '标签已存在', {
                code: 'tag_repeat'
            })
        }
        const res = await ctx.model.Tag.create({
            name,
            desc
        })
        console.log(res)
        return res
    }

    /**
     * 修改标签
     *
     */
    async update(id, payload = {}) {
        const {ctx} = this
        const tag = await ctx.model.Tag.findById(id)
        const tagName = await ctx.model.Tag.findOne({name: payload.name.trimAll()})
        if (!tag) {
            ctx.throw(404, '标签不存在', {
                code: 'tag_not'
            })
        }
        if (tagName && id !== tagName.id) {
            ctx.throw(403, '存在相同标签，无法修改', {
                code: 'tag_repeat'
            })
        }
        payload.updateAt = Date.now()

        return await ctx.model.Tag.findByIdAndUpdate(id, payload)
    }

    /*
    * 删除标签
    * */
    async destroy(id) {
        const {ctx, app} = this
        const tag = await ctx.model.Tag.findById(id)
        if (!tag) {
            ctx.throw(404, '标签不存在', {
                code: 'tag_not'
            })
        }
        if (tag.id === app.custom.defaultTag.id) {
            ctx.throw(404, '不允许删除默认标签', {
                code: 'tag_not_delete_default'
            })
        }
        return await ctx.model.Tag.findByIdAndRemove(id)
    }

}

module.exports = TagService
