'use strict'

const Service = require('egg').Service

class CommentChildrenService extends Service {
    /**
     * 查询评论
     *
     * @param {String} commentId @default null -父评论id
     * @param {Number} page @default 1 -当前页
     * @param {Number} limit @default 30 -返回条数
     * @param {String} select @default null -要显示的key
     */
    async find({commentId, page = 1, limit = 30} = {}, select = null) {
        const {ctx} = this
        const userpopulate = 'title headImg role username'

        const res = await ctx.model.CommentChildren
            .find({commentId, delete: false})
            .skip(Number(page) - 1)
            .limit(Number(limit))
            .populate('auth', userpopulate)
            .select(select)
        const count = await ctx.model.CommentChildren
            .find({commentId, delete: false})
            .count()
        return {
            count,
            page,
            list: res
        }
    }

    /**
     * 新增评论
     *
     * @param {String} articleId -文章ID
     * @param {String} commentId -父评论id
     * @param {String} content -评论内容
     */
    async create({articleId, commentId, content} = {}) {
        const {ctx, app} = this

        const comment = await ctx.model.Comment.findById(commentId)
        if (!comment) {
            ctx.throw(404, '父评论不存在', {
                code: 'parent_comments_not'
            })
        }
        if (comment.delete) {
            ctx.throw(404, '父评论已被删除', {
                code: 'parent_comments_delete'
            })
        }
        content = app.linkUsers(content)
        const res = await ctx.model.CommentChildren.create({
            articleId,
            commentId,
            content,
            auth: ctx.state.user._id
        })
        // 父级评论数+1
        ctx.model.Comment.findByIdAndUpdate(commentId, {
            $inc: {'numbers.children': 1}
        }).then(res => {
        })
        console.log(res)
        return res
    }

    /**
     * 删除评论
     *
     * @param {String} id -评论ID
     */
    async destroy(id) {
        const {ctx} = this

        const comment = await ctx.model.CommentChildren.findById(id)
        if (!comment) {
            ctx.throw(404, '评论不存在', {
                code: 'comments_not'
            })
        }
        if (comment.delete) {
            ctx.throw(404, '评论已被删除', {
                code: 'comments_delete'
            })
        }
        // 权限验证
        await ctx.powerValidate(comment.auth._id)

        const res = await ctx.model.CommentChildren.findByIdAndUpdate(id, {
            delete: true,
            updateAt: Date.now()
        })
        // 父级评论数-1
        ctx.model.Comment.findByIdAndUpdate(comment.commentId, {
            $inc: {'numbers.children': -1}
        }).then(res => {
        })
        return res
    }

    /**
     * 点赞/取消点赞评论
     *
     * @param {String} id -评论ID
     */
    async userLikeComment(id) {
        const {ctx} = this
        const isuser = await ctx.model.CommentChildren.find({'_id': id, 'likesUser': ctx.state.user._id})
        let res
        if (isuser.length === 0) {
            res = await ctx.model.CommentChildren.findByIdAndUpdate(id, {
                $addToSet: {'likesUser': ctx.state.user._id},
                $inc: {'numbers.like': 1}
            })
            return {
                message: '点赞成功',
                action: 'up'
            }
        } else {
            res = await ctx.model.CommentChildren.findByIdAndUpdate(id, {
                $pull: {'likesUser': ctx.state.user._id},
                $inc: {'numbers.like': -1}
            })
            return {
                message: '取消点赞成功',
                action: 'down'
            }
        }
    }
}

module.exports = CommentChildrenService
