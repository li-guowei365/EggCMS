'use strict';

const Service = require('egg').Service;

class RouterService extends Service {
    /**
     * 查询路由
     *
     * @param {String} id @default null -路由id 传入此值后传入的其他值将失效
     * @param {String} name @default '' -路由昵称，正则匹配
     * @param {Number} page @default 1 -当前页
     * @param {Number} limit @default 50 -返回条数
     * @param {String} select @default null -要显示的key
     */
    async find({ id, name = '', ids, page = 1, limit = 50 } = {}, select = null) {
        const { ctx } = this;
        const rolePopulate = 'name id';

        if (id) {
            return await ctx.model.Router.findById(id).select(select);
        } else if (ids) {
            return await ctx.model.Router.findOne({ ids }).select(select);
        } else {
            const res = await ctx.model.Router
                .find()
                .regex('name', new RegExp(name, 'i'))
                .skip((Number(page) - 1) * Number(limit))
                .limit(Number(limit))
                .select(select);
            const count = await ctx.model.Router
                .find()
                .regex('name', new RegExp(name, 'i'))
                .count();
            return {
                count,
                page,
                list: res
            }
        }
    }

    /**
     * 新增路由
     *
     * @param {String} methods - 请求方式
     * @param {String} name - 路由名称
     * @param {String} path - 路由地址
     */
    async create({ methods, name, path } = {}) {
        const { ctx } = this;
        const router = await ctx.model.Router.find({ name });
        if (router.length === 0) {
            return await ctx.model.Router.create({
                methods,
                name,
                path,
                ids: `${methods}_${path.substring(5)}`
            });
        } else {
            return true;
        }
    }

    /**
     * 为路由添加/移除角色
     *
     * @param {String} userId -路由id
     * @param {String} roleId -角色id
     * @param {Boolean} destroy @default false -是否移除，默认新增
     */
    async routerAndRole(routerId, roleId, destroy = false) {
        const { ctx } = this;
        const router = await ctx.model.Router.findById(routerId);
        const role = await ctx.model.Role.findById(roleId);
        console.log(router);
        console.log(role);
        if (!router) {
            ctx.throw(404, '路由不存在', {
                code: 'router_not'
            });
        }
        if (!role) {
            ctx.throw(404, '角色不存在', {
                code: 'role_not'
            });
        }
        // 权限验证
        await ctx.powerValidate();

        if (destroy) {
            // 移除
            return await ctx.model.Router.findByIdAndUpdate(routerId, {
                $pull: {
                    role: roleId
                }
            });

        } else {
            // 新增
            return await ctx.model.Router.findByIdAndUpdate(routerId, {
                $addToSet: {
                    role: roleId
                }
            });
        }

    }

    /**
     * 增加访问量
     * @param {String} ids -路由别名
     */
    async routerRequsetAdd(ids) {
        const { ctx } = this;
        return await ctx.model.Router.update(
            {
                ids
            },
            {
                $inc: {
                    reqNumber: 1
                }
            })
    }
}

module.exports = RouterService;
