const nodemailer = require('nodemailer');
const crypto = require('crypto');
const CONFIG = require('../../config/cms.config');

const mailTransporter = nodemailer.createTransport({
    service: 'qq',
    auth: {
        user: CONFIG.emailCode.qq.email,
        pass: CONFIG.emailCode.qq.code
    }

});

module.exports = {
    /**
     * @method
     * @param {string} to -邮件接收者的email地址可以同时发送多个,以逗号隔开  
     * @param {string} title -邮件标题
     * @param {string} text -文本
     * @param {string} html -邮件内容
     */
    sendEmail(options = {}) {
        const emailOptions = {
            from: CONFIG.emailCode.qq.email, // 发送者  
            to: options.to, // 接受者,可以同时发送多个,以逗号隔开  
            subject: options.title, // 标题  
            text: options.text || '', // 文本  
            html: options.html
        };
        return new Promise((resolve, reject) => {
            mailTransporter.sendMail(emailOptions, (err, info) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(info);
                }
            });
        });
    },
    /**
     * 加密算法
     * 
     * @method
     * @param {string} data - 被加密的数据
     * @param {string} password - 加密所用的密码
     * 
     *  */
    aesEncode(data, password) {
        const cipher = crypto.createCipher('aes192', password);
        let crypted = cipher.update(data, 'utf-8', 'hex');
        crypted += cipher.final('hex');
        return crypted;
    },
    /**
     * 解密算法
     * 
     * @method
     * @param {string} data - 被解密的数据
     * @param {string} password - 加密时所用的密码
     * 
     *  */
    aesDecode(data, password) {
        const decipher = crypto.createDecipher('aes192', password);
        let decrypted = decipher.update(data, 'hex', 'utf-8');
        decrypted += decipher.final('utf-8');
        return decrypted;
    },
    /**
     * 判断是否为json
     * 
     * @method
     * @param {string} data - json字符串
     * 
     *  */
    isJson(data) {
        if (typeof (data) === 'string') {
            try {
                return JSON.parse(data);
            } catch (e) {
                return false;
            }
        } else {
            return false;
        }
    },
    /**
     * sort查询格式化
     * @param {Array | undefined} sort - -1为倒序，+1为正序
     */
    sortFormat(sort) {
        let sorts = {
            createAt: -1
        }
        if (Array.isArray(sort)) {
            sorts = {};
            for (let i of sort) {
                sorts[i.key] = i.value;
            }
        }
        return sorts;
    },
    /**
     * query参数格式化
     * @param {Object} opt
     */
    queryFormat(opt) {
        let query = {
            delete: false
        }
        for (let key in opt) {
            const value = opt[key];
            if (query[key]) {
                if (Array.isArray(value) && value.length > 0) {
                    query[key] = { $in: value };
                } else {
                    query[key] = value;
                }
            }
        }
        return query;
    }
}