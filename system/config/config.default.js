'use strict';
const path = require('path');
const CMSCONFIG = require('./cms.config')
module.exports = appInfo => {
    const config = exports = {};

    // use for cookie sign key, should change to your own and keep security
    config.keys = appInfo.name + '_1522075092508_5168';

    // add your config here
    //  'historyApi'
    config.middleware = ['errorHandler', 'koaStatic'];

    // mongoodb
    config.mongoose = {
        url: CMSCONFIG.db.url,
        options: {}
    };
    // koa-static
    config.koaStatic = {
        root: path.resolve(__dirname, '../app/www'),
        option: {}
    };
    // sessionToken
    config.jwt = {
        secret: CMSCONFIG.jwt.secret
    };
    // cors
    // config.cors = {
    //     // origin: '*',
    //     // allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH'
    // };
    config.security = {
        domainWhiteList: CMSCONFIG.domainWhiteList,
        xframe: {
            enable: false
        },
        xss: {
            enable: false
        },
        xst: {
            enable: false
        },
        csrf: {
            enable: false,
            ignoreJSON: true
        },
    };
    config.static = {
        prefix: '/public/',
        dir: path.join(appInfo.baseDir, 'app/public'),
        // support lazy load
        dynamic: true,
        preload: false,
        buffer: false
    };
    return config;
};
