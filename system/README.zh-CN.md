# eggCMS


## 快速入门

<!-- 在此次添加使用文档 -->

如需进一步了解，参见 [egg 文档][egg]。

### 本地开发

```bash
$ npm i
$ npm run dev
$ open http://localhost:7001/
```

### 部署

```bash
$ npm start
$ npm stop
```

### 单元测试

- [egg-bin] 内置了 [mocha], [thunk-mocha], [power-assert], [istanbul] 等框架，让你可以专注于写单元测试，无需理会配套工具。
- 断言库非常推荐使用 [power-assert]。
- 具体参见 [egg 文档 - 单元测试](https://eggjs.org/zh-cn/core/unittest)。

### 内置指令

- 使用 `npm run lint` 来做代码风格检查。
- 使用 `npm test` 来执行单元测试。
- 使用 `npm run autod` 来自动检测依赖更新，详细参见 [autod](https://www.npmjs.com/package/autod) 。


[egg]: https://eggjs.org

### 完成进度
#### 增加配置项  
整合**数据库配置，初始化时管理员账号密码配置，jwt配置，跨域白名单配置于** 于`cms.config.js`  

### 功能列表
1. 邮箱注册  
2. 邮箱绑定  

### 消息列表中的`content` 字段

类型：`Array`

示例：用户 xxx 在文章 xxx 中提到了你

``` js
content: [{
    type: 'string',
    value: '用户',
}, {
    type: 'user',
    relationId: ctx.state.user._id,
    value: ctx.state.user.name,
}, {
    type: 'string',
    value: '在文章',
}, {
    type: 'article',
    relationId: articleId,
    value: res.title,
}, {
    type: 'string',
    value: '中提到了你'
}]
```

type类型：

`string` 仅代表字符串

`user ` 代表被提到的用户

`article` 代表文章信息

`commentChildren` 代表来自二级评论

`comment` 代表来自一级评论



`relationId` 关联的id 


## 错误码
### 422
传入参数错误

### 401
用户不存在

### 403
没有使用权限