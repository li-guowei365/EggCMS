'use strict';

module.exports = (app) => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const MessageSchema = new Schema({
        /**
         * type = 0 -系统消息
         *
         */
        type: {
            type: String,
            require: true
        },
        content: {
            type: String,
            require: true,
            default: ''
        },
        /**
         * 消息状态
         * statu = 0 -未读
         * statu = 1 -已读
         */
        state: {
            type: Number,
            default: 0
        },
        auth: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        // 接收人
        sendee: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        createAt: {
            type: Date,
            default: Date.now
        },
        updateAt: {
            type: Date,
            default: Date.now
        }
    });

    return mongoose.model('message', MessageSchema);
};
