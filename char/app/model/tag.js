'use strict';

module.exports = (app) => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const TagSchema = new Schema({
        name: {
            type: String,
            require: true,
            unique: true
        },
        default: {
            type: Boolean,
            default: false
        },
        desc: {
            type: String,
            default: ''
        },
        createAt: {
            type: Date,
            default: Date.now
        },
        updateAt: {
            type: Date,
            default: Date.now
        }
    });

    return mongoose.model('tag', TagSchema);
};
