'use strict';

module.exports = (app) => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const RoleSchema = new Schema({
        name: {
            type: String,
            unique: true,
            require: true,
            index: 1
        },
        desc: {
            type: String,
            desc: ''
        },
        routers: [
            {
                type: Schema.Types.ObjectId,
                ref: 'router'
            }
        ],
        createAt: {
            type: Date,
            default: Date.now
        },
        updateAt: {
            type: Date,
            default: Date.now
        }
    });
    // 对角色描述进行处理
    RoleSchema.pre('save', function (next) {
        let role = this;
        if (!role.desc) {
            role.desc = role.name;
        }
        next();
    });
    return mongoose.model('role', RoleSchema);
};
