'use strict';

module.exports = (app) => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;
    
    const CommentSchema = new Schema({
        articleId: {
            type: Schema.Types.ObjectId,
            ref: 'article'
        },
        content: {
            type: String,
            require: true
        },
        auth: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        likesUser: [
            {
                type: Schema.Types.ObjectId,
                ref: 'user'
            }
        ],
        numbers: {
            like: {
                type: Number,
                default: 0
            },
            children: {
                type: Number,
                default: 0
            }
        },
        delete: {
            type: Boolean,
            default: false
        },
        createAt: {
            type: Date,
            default: Date.now
        },
        updateAt: {
            type: Date,
            default: Date.now
        }
    });

    return mongoose.model('comment', CommentSchema);
};
