'use strict';

module.exports = (app) => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const NoticeSchema = new Schema({
        title: {
            type: String,
            require: true
        },
        content: {
            type: String,
            require: true
        },
        auth: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        lookUser: [
            {
                type: Schema.Types.ObjectId,
                ref: 'user'
            }
        ],
        createAt: {
            type: Date,
            default: Date.now
        },
        updateAt: {
            type: Date,
            default: Date.now
        }
    });

    return mongoose.model('notice', NoticeSchema);
};
