'use strict';

module.exports = (app) => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const ArticleSchema = new Schema({
        title: {
            type: String,
            require: true
        },
        content: {
            type: String,
            require: true
        },
        desc: {
            type: String
        },
        auth: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        tag: [{
            type: Schema.Types.ObjectId,
            ref: 'tag'
        }],
        likesUser: [
            {
                type: Schema.Types.ObjectId,
                ref: 'user'
            }
        ],
        isLike: {
            type: Boolean,
            default: false
        },
        top: {
            type: Boolean,
            require: false
        },
        numbers: {
            like: {
                type: Number,
                default: 0
            },
            look: {
                type: Number,
                default: 0
            },
            share: {
                type: Number,
                default: 0
            },
            comment: {
                type: Number,
                default: 0
            }
        },
        // 文章状态 1上架 2下架
        state: {
            type: String,
            default: '1'
        },
        delete: {
            type: Boolean,
            default: false
        },
        next: {
            type: Object,
            default: null
        },
        prev: {
            type: Object,
            default: null
        },
        createAt: {
            type: Date,
            default: Date.now
        },
        updateAt: {
            type: Date,
            default: Date.now
        }
    });

    return mongoose.model('article', ArticleSchema);
};
