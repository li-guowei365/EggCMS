'use strict'

const koaStatic = require('koa-static');

module.exports = (options, app) => {
    return koaStatic(options.root, options.option);
}