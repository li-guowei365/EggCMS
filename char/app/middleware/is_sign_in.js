'use strict';

module.exports = (option, app) => {
    return async function (ctx, next) {
        const userSign = await app.userSign(ctx);
        
        if(userSign.error){
            // 未登录
            ctx.throw(userSign.status, userSign.message);
        }else{
            // 已登陆
            await next();
        }
    }
};