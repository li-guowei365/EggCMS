'use strict';

const Controller = require('egg').Controller;
const SELECT = require('../../../../config/select');
class RoleController extends Controller {
    /**
     * 获取角色
     */
    async find() {
        const { ctx } = this;
        const { id, name, page, currentPage } = ctx.query;
        const select = SELECT.ROLE;
        const Rules = {
            id: {
                type: 'objectId',
                required: false
            }
        };
        ctx.proving(Rules, ctx.query);
        const role = await ctx.service.role.find({ id, name, page, currentPage }, select);
        ctx.success(role);
    }

    /**
     * 修改角色
     */
    async update() {
        const { ctx } = this;
        const body = ctx.request.body;
        const { id, name, desc } = body;
        const roleRules = {
            name: {
                type: 'string',
                keyname: '角色名称'
            },
            id: {
                type: 'string',
                keyname: 'ID'
            }
        };
        ctx.proving(roleRules);
        // 角色权限认证
        await ctx.powerValidate();
        const role = await ctx.service.role.update(id, { name, desc });
        ctx.success({
            message: '修改成功'
        });
    }

    /**
     * 新增角色
     */
    async create() {
        const { ctx } = this;
        const body = ctx.request.body;
        const name = body.name;

        const roleRules = {
            name: {
                type: 'string',
                keyname: '角色名称'
            }
        };
        ctx.proving(roleRules);
        // 角色权限认证
        await ctx.powerValidate();

        const role = await ctx.service.role.create(name);
        ctx.success({ id: role.id });
    }

    /*
    * 删除角色
    * */
    async destroy() {
        const { ctx } = this;
        const id = ctx.query.id;
        const Rules = {
            id: {
                type: 'string',
                keyname: 'ID'
            }
        };

        ctx.proving(Rules, ctx.query);
        // 角色权限认证
        await ctx.powerValidate();

        const user = await ctx.service.role.destroy(id);
        ctx.success({
            message: '删除成功'
        });
    }

    /*
    * 为角色添加路由
    */
    async createRouter() {
        // roterCreateRouter
        const { ctx } = this;
        const body = ctx.request.body;
        const { roleId, routerId } = body;
        const roleRules = {
            roleId: {
                type: 'objectId',
                keyname: '角色ID'
            },
            routerId: {
                type: 'objectId',
                keyname: '路由ID'
            }
        };
        ctx.proving(roleRules);
        // 角色权限认证
        await ctx.powerValidate();

        await ctx.service.role.roterCreateRouter(roleId, routerId, true);
        ctx.success({
            message: '添加成功',
        });
    }

    /*
    * 为角色移除路由
    */
   async destroyRouter() {
    const { ctx } = this;
    const body = ctx.request.body;
    const { roleId, routerId } = body;
    const roleRules = {
        roleId: {
            type: 'objectId',
            keyname: '角色ID'
        },
        routerId: {
            type: 'objectId',
            keyname: '路由ID'
        }
    };
    ctx.proving(roleRules);
    // 角色权限认证
    await ctx.powerValidate();
    
    await ctx.service.role.roterCreateRouter(roleId, routerId, false);
    ctx.success({
        message: '移除成功',
    });
}

}

module.exports = RoleController;
