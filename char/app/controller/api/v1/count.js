'use strict'
const Controller = require('../base.js');
class CountController extends Controller {
    async index() {
        const {ctx} = this;
        const res = await ctx.service.count.index()
        ctx.success(res)
    }
}
module.exports = CountController;