'use strict';

const Controller = require('../base.js');
const SELECT = require('../../../../config/select');

class NoticeController extends Controller {
    /**
     *列表
     */
    async find() {
        const {ctx} = this;
        const {title,page, sort, currentPage} = ctx.query;
        const select = SELECT.NOTICE;
        const res = await ctx.service.notice.find({title, sort, page, currentPage}, select);
        ctx.success(res);
    }

    /**
     * 新增
     */
    async create() {
        const {ctx} = this;
        const body = ctx.request.body;
        const {title, content} = body;
        const Rules = {
            title: {
                type: 'string',
                keyname: '公告标题'
            },
            content: {
                type: 'string',
                keyname: '公告内容'
            }
        };
        ctx.proving(Rules);

        const res = await ctx.service.notice.create({title, content});
        ctx.success(res);
    }

    /**
     * 修改
     */
    async update() {
        const {ctx} = this;
        const body = ctx.request.body;
        const {id, title, content} = body;
        const Rules = {
            title: {
                type: 'string',
                keyname: '公告标题'
            },
            content: {
                type: 'string',
                keyname: '公告内容'
            }
        };
        ctx.proving(Rules);

        const res = await ctx.service.notice.update(id,{title, content});
        ctx.success(res);
    }

    /*
     * 删除
     * */
    async destroy() {
        const {ctx} = this;
        const body = ctx.request.body;
        const {id} = body.id;
        await ctx.service.notice.destroy(id);
        ctx.success({
            message: '删除成功'
        });
    }
}

module.exports = NoticeController;
