'use strict';

const Controller = require('../base.js');
const SELECT = require('../../../../config/select');
class CommentController extends Controller {
	/**
     *评论获取
     */
	async find() {
		const { ctx } = this;
		const { articleId, page, currentPage } = ctx.query;

		const Rules = {
			articleId: {
				type: 'objectId',
				keyname: '文章ID'
			}
		};
		ctx.proving(Rules, ctx.query);

		const select = SELECT.COMMENT;
		const res = await ctx.service.comment.find({ articleId, page, currentPage }, select);
		ctx.success(res);
	}

	/**
     * 新增评论
     */
	async create() {
		const { ctx, app } = this;
		const body = ctx.request.body;
		const articleId = body.articleId;
		let content = body.content;
		console.log('登录者信息', ctx.state.user);

		const Rules = {
			articleId: {
				type: 'string',
				keyname: '文章ID'
			},
			content: {
				type: 'string',
				keyname: '评论内容'
			}
		};
		ctx.proving(Rules);
		// 取出被@的用户
		let sendees = app.fetchUsers(content);
		// 将文章内容中被@的用户进行格式化
		content = app.linkUsers(content);
		// 写入数据库
		const comment = await ctx.service.comment.create({
			articleId,
			content
		});

		ctx.service.article.find({ id: articleId}, SELECT.ARTICLE_DETAIL).then((res) => {
			console.log('评论文章时获取到的文章信息：', res);
			// 给被@的用户发消息
			this.submitMsg({
				sendees,
				content: `用户<a href="#/user/${ctx.state.user._id}">${ctx.state.user.name}</a>在文章<a href="#/article/${articleId}">${res.title}</a>中提到了你`,
				type: 0
			});
			// 给文章作者发消息
			this.submitMsg({
				sendees: [ res.auth.username ],
				content: `用户<a href="#/user/${ctx.state.user._id}">${ctx.state.user.name}</a>评论了您的文章<a href="#/article/${articleId}">${res.title}</a>`,
				type: 0
			});
		});
		const select = SELECT.COMMENT;
		const cres = await ctx.service.comment.findById(comment.id, select);
		ctx.success(cres);
	}

	/*
     * 删除评论
     * */
	async destroy() {
		const { ctx } = this;
		const body = ctx.request.body;
		const id = body.id;

		const Rules = {
			id: {
				type: 'objectId',
				keyname: '评论ID'
			}
		};
		ctx.proving(Rules);

		await ctx.service.comment.destroy(id);

		ctx.success({
			message: '删除成功'
		});
	}

	/*
     * 点赞，取消点赞
     * */
	async likeComment() {
		const { ctx } = this;
		const body = ctx.request.body;
		const id = body.id;
		const Rules = {
			id: {
				type: 'objectId',
				keyname: '评论ID'
			}
		};
		ctx.proving(Rules);
		const res = await ctx.service.comment.userLikeComment(id);
		ctx.success(res);
	}
}

module.exports = CommentController;
