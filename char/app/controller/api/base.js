'use strict';

const Controller = require('egg').Controller;

class BaseController extends Controller {
	/**
     * 发送消息
     *
     * @param {Array}  sendess -被@的用户
     * @param {String} content -消息内容
     * @param {Number} type    -消息类型
     */
	async submitMsg({ sendees = [], content, type }) {
		const { ctx } = this;
		const loginuser = ctx.state.user.username;
		const loginuser_index = sendees.indexOf(loginuser);
		if (sendees.includes(loginuser)) {
			sendees.splice(loginuser_index, 1);
			console.log(`去除被@用户中的自己`);
		}
		for (let sendee of sendees) {
			try {
				await ctx.service.message.create({
					content,
					type,
					username: sendee
				});
				console.log(`向${sendee}发送消息成功`);
			} catch (e) {
				console.log(`向${sendee}发送消息失败`, e);
			}
		}
	}
}

module.exports = BaseController;
