'use strict';

/**
 * @param {Egg.Application} app - egg application
 */

module.exports = (app) => {
	const { router, controller } = app;
	const isSignIn = app.middleware.isSignIn({}, app);
	router.get('/', controller.home.index);
	// 给需要增加配置权限的路由后面拼接_power，并且保证路由名称的唯一性

	//用户
	router.get('用户列表_power', '/api/v1/user', isSignIn, controller.api.v1.user.find);
	router.get('超级管理员信息', '/api/v1/admininfo', controller.api.v1.user.adminInfo);
	router.get('当前登陆用户信息_power', '/api/v1/userinfo', isSignIn, controller.api.v1.user.userInfo);
	router.post('新增用户_power', '/api/v1/user', isSignIn, controller.api.v1.user.create);
	router.put('修改用户_power', '/api/v1/user', isSignIn, controller.api.v1.user.update);
	router.put('封禁，解封用户_power', '/api/v1/userStatus', isSignIn, controller.api.v1.user.userStatus);
	router.put('重置密码_power', '/api/v1/userRestPassword', isSignIn, controller.api.v1.user.restPassword);
	router.delete('删除用户_power', '/api/v1/user', isSignIn, controller.api.v1.user.destroy);

	// 获取文章数量，标签数量
	router.get('数量统计', '/api/v1/count', controller.api.v1.count.index)

	//角色
	router.get('角色列表_power', '/api/v1/role', isSignIn, controller.api.v1.role.find);
	router.post('新增角色_power', '/api/v1/role', isSignIn, controller.api.v1.role.create);
	router.put('修改角色_power', '/api/v1/role', isSignIn, controller.api.v1.role.update);
	router.delete('删除角色_power', '/api/v1/role', isSignIn, controller.api.v1.role.destroy);

	// 为角色增添路由
	router.post('为角色增加权限_power', '/api/v1/roleAddRouter', isSignIn, controller.api.v1.role.createRouter);
	router.post('为角色移除权限_power', '/api/v1/roleDestroyRouter', isSignIn, controller.api.v1.role.destroyRouter);
	
	//路由
	router.get('路由列表_power', '/api/v1/router', isSignIn, controller.api.v1.router.find);

	// 用户与角色
	router.post('赋予用户角色_power', '/api/v1/userAddRole', isSignIn, controller.api.v1.user.addRole);
	router.post('移除用户角色_power', '/api/v1/destroyUserRole', isSignIn, controller.api.v1.user.destroyRole);

	// 登录登出
	router.post('登录', '/api/v1/signIn', controller.api.v1.sign.index);
	router.post('退出登录', '/api/v1/signOut', isSignIn, controller.api.v1.sign.out);

	// 消息
	router.get('消息列表', '/api/v1/message', isSignIn, controller.api.v1.message.find);
	router.delete('删除消息', '/api/v1/message', isSignIn, controller.api.v1.message.destroy);
	router.put('设置消息为已读', '/api/v1/messageRead', isSignIn, controller.api.v1.message.read);

	// 文章
	router.get('文章列表', '/api/v1/article', controller.api.v1.article.find);
	router.get('文章详情', '/api/v1/articleDetail', controller.api.v1.article.dateail);
	router.post('新增文章', '/api/v1/article', isSignIn, controller.api.v1.article.create);
	router.put('修改文章', '/api/v1/article', isSignIn, controller.api.v1.article.update);
	router.put('文章置顶与取消置顶_power', '/api/v1/articleTopToggle', isSignIn, controller.api.v1.article.toggleTop);
	router.put('文章状态变更_power', '/api/v1/articleUpdateState', isSignIn, controller.api.v1.article.updateState);
	router.post('收藏或取消收藏文章', '/api/v1/likeArticle', isSignIn, controller.api.v1.article.likeArticle);
	router.delete('删除文章_power', '/api/v1/article', isSignIn, controller.api.v1.article.destroy);

	// 评论
	router.get('评论列表', '/api/v1/comment', controller.api.v1.comment.find);
	router.post('新增评论', '/api/v1/comment', isSignIn, controller.api.v1.comment.create);
	router.post('点赞或取消点赞评论', '/api/v1/likeComment', isSignIn, controller.api.v1.comment.likeComment);
	router.delete('删除评论_power', '/api/v1/comment', isSignIn, controller.api.v1.comment.destroy);

	// 子评论
	router.get('子评论列表', '/api/v1/commentChildren', controller.api.v1.commentChildren.find);
	router.post('新增子评论', '/api/v1/commentChildren', isSignIn, controller.api.v1.commentChildren.create);
	router.post('点赞或取消点赞子评论', '/api/v1/likecommentChildren', isSignIn, controller.api.v1.commentChildren.likeComment);
	router.delete('删除子评论_power', '/api/v1/commentChildren', isSignIn, controller.api.v1.commentChildren.destroy);

	// 文章标签
	router.get('文章标签列表', '/api/v1/tag', controller.api.v1.tag.find);
	router.post('新增标签_power', '/api/v1/tag', isSignIn, controller.api.v1.tag.create);
	router.put('修改标签_power', '/api/v1/tag', isSignIn, controller.api.v1.tag.update);
	router.delete('删除标签_power', '/api/v1/tag', isSignIn, controller.api.v1.tag.destroy);

	// 文章标签
	router.get('广告列表', '/api/v1/banner', controller.api.v1.banner.find);
	router.post('新增广告_power', '/api/v1/banner', isSignIn, controller.api.v1.banner.create);
	router.put('广告状态变更_power', '/api/v1/bannerUpdateState', isSignIn, controller.api.v1.banner.updateState);
	router.put('修改广告_power', '/api/v1/banner', isSignIn, controller.api.v1.banner.update);
	router.delete('删除广告_power', '/api/v1/banner', isSignIn, controller.api.v1.banner.destroy);
	
	// 文件上传
	router.post('文件上传', '/api/v1/fileUpload', isSignIn, controller.api.v1.file.head);
};