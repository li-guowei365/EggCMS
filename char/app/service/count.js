'use strict'
const Service = require('egg').Service;

// 数据统计
class CountService extends Service {
    async index() {
        const { ctx } = this;
        const ArticleCount = await ctx.model.Article.find({ delete: false }).count();
        const TagCount = await ctx.model.Tag.count();
        return {
            article: ArticleCount,
            tag: TagCount
        }
    }
}

module.exports = CountService;