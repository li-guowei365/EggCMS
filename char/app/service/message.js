'use strict';

const Service = require('egg').Service;

class MessageService extends Service {
	/*
     * 获取消息
     * */
	async findMsg({ id, page = 1, currentPage = 10 }, select = null) {
		const { ctx } = this;

		if (id) {
			return await ctx.model.Message.findById(id).select(select);
		} else {
			const sendee = ctx.state.user._id;
			const res = await ctx.model.Message
				.find({ sendee })
				.skip(Number(page) - 1)
				.limit(Number(currentPage))
				.select(select);
			const count = await ctx.model.Message.find({ sendee }).count();
			return {
				count,
				page,
				list: res
			};
		}
	}
	/**
     * 发送消息
     */
	async create({ username, content, type }) {
		const { ctx } = this;
		const sendee = await ctx.model.User.findOne({ username });
		console.log(sendee);
		if (sendee) {
			return await ctx.model.Message.create({
				content,
				type,
				auth: ctx.state.user._id,
				sendee: sendee.id
			});
		} else {
			return null;
		}
	}

	/**
     * 删除消息
     *
     * @param {String} id -消息ID
     */
	async destroy(id) {
		const { ctx } = this;

		const message = await ctx.model.Message.findById(id);
		if (!message) {
			ctx.throw(404, '消息不存在或已被删除');
		}
		// 权限验证
		await ctx.powerValidate(message.auth._id);

		const res = await ctx.model.Message.findByIdAndRemove(id);
		return res;
	}

	/**
     * 消息标记为已读
     *
     * @param {String} id -消息ID
     */
	async read(id) {
		const { ctx } = this;

		const message = await ctx.model.Message.findById(id);
		if (!message) {
			ctx.throw(404, '消息不存在或已被删除');
		}

		const msgState = {
			state: 1,
			updateAt: Date.now()
		};
		const res = await ctx.model.Message.findByIdAndUpdate(id, msgState);

		return res;
	}
	/**
     * 消息全部标记为已读
     *
     */
	async readAll() {
		const { ctx } = this;
		const msgState = {
			state: 1,
			updateAt: Date.now()
		};
		const res = await ctx.model.Message.update({ auth: ctx.state.user._id, state: 0 }, msgState, {
			multi: false,
			overwrite: true
		});
		return res;
	}
}
module.exports = MessageService;
