'use strict';

module.exports = {
	/*
    * 日期格式化
    * */
	dateFormat(date, fmt = 'YY-MM-DD HH:MM:SS') {
		function check(str) {
			str = str.toString();
			if (str.length < 2) {
				str = '0' + str;
			}
			return str;
		}

		let mat = {};
		mat.M = date.getMonth() + 1; //月份记得加1
		mat.H = date.getHours();
		mat.s = date.getSeconds();
		mat.m = date.getMinutes();
		mat.Y = date.getFullYear();
		mat.D = date.getDate();
		mat.d = date.getDay(); //星期几
		mat.d = check(mat.d);
		mat.H = check(mat.H);
		mat.M = check(mat.M);
		mat.D = check(mat.D);
		mat.s = check(mat.s);
		mat.m = check(mat.m);

		return mat.Y + '-' + mat.M + '-' + mat.D + ' ' + mat.H + ':' + mat.m + ':' + mat.s;
	},
	/*
    * 获取所有路由
    * */
	get getRouters() {
		return this.router.stack.map((item) => {
			const methods = item.methods.filter((item) => {
				return item !== 'HEAD';
			});
			return {
				methods: methods[0],
				name: item.name,
				path: item.path,
				id: `${methods[0]}_${item.path.substring(5)}`
			};
		});
	},
	// 登陆验证
	async userSign(ctx) {
		const token = ctx.request.header.token || '';
		// 验证token是否存在
		if (!token) {
			return {
				error: true,
				status: 401,
				message: 'token不存在，请登录'
			};
		}
		// 验证token是否合法
		console.log(token)
		let tokenMatch = false
		try{
			tokenMatch = await this.jwt.verify(token, this.config.jwt.secret);
		}catch(e){
			console.log(e)
			return {
				error: true,
				status: 401,
				message: '身份过期，请重新登录'
			};
		}
		if (!tokenMatch) {
			return {
				error: true,
				status: 401,
				message: '身份过期，请重新登录'
			};
		}
		// 查询token所属用户并校验
		const { id } = await this.jwt.decode(token);
		const user = await ctx.service.user.find({ id });
		// console.log(user);
		if (!user) {
			return {
				error: true,
				status: 401,
				message: '身份校验失败，用户不存在'
			};
		} else if (user.delete) {
			return {
				error: true,
				status: 401,
				message: '身份校验失败，用户已被封禁'
			};
		}
		{
			console.log('用户已登陆，身份校验成功');
			ctx.state.user = user;
			return user;
		}
	},
	/**
     * 使用cnode中的取出@用户方法
     * https://github.com/cnodejs/egg-cnode/blob/master/app/service/at.js
     * @param {String} text 文本内容
     * @return {Array} 用户名数组
     */
	fetchUsers(text) {
		if (!text) {
			return [];
		}
		const ignoreRegexs = [
			/```.+?```/g, // 去除单行的 ```
			/^```[\s\S]+?^```/gm, // ``` 里面的是 pre 标签内容
			/`[\s\S]+?`/g, // 同一行中，`some code` 中内容也不该被解析
			/^ {4}.*/gm, // 4个空格也是 pre 标签，在这里 . 不会匹配换行
			/\b\S*?@[^\s]*?\..+?\b/g, // somebody@gmail.com 会被去除
			/\[@.+?\]\(\/.+?\)/g // 已经被 link 的 username
		];

		ignoreRegexs.forEach((ignore_regex) => {
			text = text.replace(ignore_regex, '');
		});
		const results = text.match(/@[a-z0-9\-_]+\b/gim);
		const names = [];
		if (results) {
			for (let i = 0, l = results.length; i < l; i++) {
				let s = results[i];
				// remove leading char @
				s = s.slice(1);
				names.push(s);
			}
		}
		return [ ...new Set(names) ];
	},
	/**
     * 根据文本内容，替换为数据库中的数据
     * @param {String} text 文本内容
     * @return {String} 替换后的文本内容
     */
	linkUsers(text) {
		const users = this.fetchUsers(text);
		for (let i = 0; i < users.length; i++) {
			const name = users[i];
			text = text.replace(new RegExp('@' + name + '\\b(?!\\])', 'g'), '[@' + name + '](/user/' + name + ')');
		}
		return text;
	}
};
