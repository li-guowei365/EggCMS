'use strict';
const _array = require('lodash/array');
module.exports = {
	/**
     * proving data with rules
     *
     * @param  {Object} rules  - proving rule object, see [parameter](https://github.com/node-modules/parameter)
     * @param  {Object} [data] - proving target, default to `this.request.body`
     */
	proving(rules, data) {
		data = data || this.request.body;
		const errors = this.app.validator.validate(rules, data);
		this.app.validator.addRule('jsonString', (rule, value) => {
			try {
				JSON.parse(value);
			} catch (err) {
				return 'must be json string';
			}
		});
		if (errors) {
			const msgoption = {
				required: '${name}不能为空',
				'should not be empty': '${name}不能为空',
				'should be an email': '${name}不符合email格式要求',
				'should be a number': '${name}必须是一个数字',
				'should match /^[0-9a-zA-Z]+$/': '${name}只能是英文或者数字'
			};
			// 自定义返回中文提示
			errors.map((item) => {
				const keyname = rules[item.field].keyname;
				if (keyname) {
					if (item.message.includes('length should bigger')) {
						item.message_cn = keyname + `长度最少为${rules[item.field].min}位`;
					} else if (item.message.includes('length should smaller')) {
						item.message_cn = keyname + `长度最大为${rules[item.field].max}位`;
					} else {
						item.message_cn = msgoption[item.message].replace(/\${name}/g, keyname);
					}
				}
				return item;
			});
			console.log('%c 出现了错误', 'color:red', errors);
			this.throw(422, 'Validation Failed', {
				code: 'invalid_param',
				errors
			});
		}
	},
	success(data) {
		this.status = 200;
		this.body = data;
	},
	/*
     * 角色权限验证
     * */
	async powerValidate(id) {
		const { app, method, path, routerName, state } = this;
		const user = state.user;
		// 判断是否登陆
		if (!user) {
			this.throw(401, '用户不存在，请登录');
		}

		const { role: userRole, _id: userid, root } = user;
		console.log('登录用户Id', String(userid));
		console.log('传入用户Id', String(id));
		if (id && String(userid) === String(id)) {
			// 验证是否为本人
			console.log('用户本人，通过');
			return user;
		} else if (userRole.length === 0) {
			// 验证是否存在角色
			this.throw(403, '您没有使用权限');
		} else {
			// 当root为true时代表用户为最初的超级管理员
			// 验证是否为超级管理员
			if (root) {
				console.log(user.username + '是超级管理员直接通过');
				return user;
			}
			for (let i of userRole) {
				if (i.name === '超级管理员') {
					console.log(user.username + '是超级管理员直接通过');
					return user;
				}
			}
			// 当不是超级管理员时，验证用户角色是否符合此路由要求
			console.log(`用户不是超级管理员，验证用户角色是否符合此路由要求`);
			// 获取当前路由的id
			const routerModel = await this.model.Router.findOne({ name: routerName });

			if (!routerModel) {
				this.throw(403, `未查询到此路由，所以您无法进行此操作`);
			}
			// 获取当前用户的所有的角色
			let rs = []
			for (let r of user.role) {
				const id = r.id
				if (!rs.includes(id)) {
					rs.push(id)
				}
			}
			const roles = await this.model.Role.find({ _id: { $in: rs } }).populate('routers', 'id');
			// 取出角色中包含的路由列表
			let rts = []
			for (let i of roles) {
				for (let j of i.routers) {
					console.log(j)
					const rid = j.id.toString();
					if (!rts.includes(rid)) {
						rts.push(rid)
					}
				}
			}
			// 判断角色中的路由是否存在当前路由
			if (!rts.includes(routerModel.id)) {
				this.throw(403, `您没有使用权限`);
			} else {
				console.log(`用户角色为符合此路由要求，验证通过`);
				return user;
			}
		}
	}
};
