'use strict';

// had enabled by egg
// exports.static = true;

exports.mongoose = {
	enable: true,
	package: 'egg-mongoose'
};

// cors跨域
exports.cors = {
	enable: true,
	package: 'egg-cors'
};

// sessiontoken
exports.jwt = {
	enable: true,
	package: 'egg-jwt'
};

exports.validate = {
	enable: true,
	package: 'egg-validate'
};

exports.io = {
	enable: true,
	package: 'egg-socket.io',
}