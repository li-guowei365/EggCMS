// 返回给前台的字段

module.exports = {
    USER: 'username name root phone qq weixin sex email state summary headImg role createAt ',
    ARTICLE: 'title desc auth tag top numbers createAt state updateAt',
    ARTICLE_DETAIL: 'title content desc auth tag top numbers createAt updateAt likesUser isLike',
    COMMENT: 'articleId content auth numbers createAt',
    COMMENT_CHILDREN: 'articleId commentId content auth numbers createAt',
    MESSAGE: 'type content state auth sendee createAt',
    ROLE: 'name desc routers createAt',
    ROUTER: 'methods name path ids role createAt',
    NOTICE: 'title content auth createAt',
    BANNER: 'title desc type state href img auth createAt',
    TAG: 'name desc createAt'
}
