module.exports = {
    db: {
        url: 'mongodb://127.0.0.1/eggcms'
    },
    admin: {
        username: 'admin', // 登录用户名，只能是英文或数字
        name: '阿炸克斯', // 超级管理员昵称
        password: '123456', // 登录密码
    },
    jwt: {
        secret: 'eggcms2018', // jwt加密时的secret
    },
    domainWhiteList: [ // 允许跨域访问白名单
        'http://localhost:8080',
        'http://127.0.0.1:8080',
        'http://localhost:80',
        'http://localhost:7001',
    ]
}