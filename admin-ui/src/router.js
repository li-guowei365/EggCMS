import Vue from 'vue'
import Router from 'vue-router'
import Main from './views/main'
import Article from './views/article'
import ceArticle from './views/article/ce-article'
import Banner from './views/banner'
import Home from './views/home'
import Liuyan from './views/liuyan'
import Message from './views/message'
import Notice from './views/notice'
import Role from './views/role'
import Routers from './views/routers'
import Tag from './views/tag'
import User from './views/user'
import ApiHistory from './views/system/api_history'
import Log from './views/system/log'
import Resource from './views/system/resource'
import Login from './views/login'

Vue.use(Router)
export default new Router({
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                hideMenu: true
            }
        },
        {
            path: '/',
            name: 'main',
            redirect: '/home',
            component: Main,
            children: [
                {
                    path: 'home',
                    name: 'home',
                    component: Home,
                    meta: {
                        icon: 'remix-icon-home-8-line',
                        title: '首页',
                        hideMenu: false,
                    },
                },
            ]
        },
        {
            path: '/user',
            name: 'user',
            component: Main,
            meta: {
                icon: 'remix-icon-group-line',
                title: '用户管理',
                hideMenu: false,
            },
            children: [
                {
                    path: '/user_page',
                    name: 'user_page',
                    component: User,
                    meta: {
                        icon: 'remix-icon-group-line',
                        title: '用户管理',
                        hideMenu: false,
                    },
                }
            ]
        },
        {
            path: '/role',
            name: 'role',
            component: Main,
            children: [
                {
                    path: '/role_page',
                    name: 'role_page',
                    component: Role,
                    meta: {
                        icon: 'remix-icon-user-received-line',
                        title: '角色管理',
                        hideMenu: false,
                    },
                },
            ]
        },
        {
            path: '/routers',
            name: 'routers',
            component: Main,
            children: [
                {
                    path: '/routers_page',
                    name: 'routers_page',
                    component: Routers,
                    meta: {
                        icon: 'remix-icon-git-merge-line',
                        title: '路由管理',
                        hideMenu: false,
                    },
                },
            ]
        },
        {
            path: '/article',
            name: 'article',
            component: Main,
            meta: {
                icon: 'remix-icon-list-check-2',
                title: '文章管理',
                hideMenu: false,
            },
            children: [
                {
                    path: '/article_page',
                    name: 'article_page',
                    component: Article,
                    meta: {
                        title: '文章列表',
                        hideMenu: false,
                    },
                },
                {
                    path: '/article_ce',
                    name: 'cearticle_ce',
                    component: ceArticle,
                    meta: {
                        title: '新增文章',
                        hideMenu: false,
                    },
                }
            ]
        },
        {
            path: '/tag',
            name: 'tag',
            component: Main,
            children: [
                {
                    path: '/tag_page',
                    name: 'tag_page',
                    component: Tag,
                    meta: {
                        icon: 'remix-icon-layout-column-line',
                        title: '栏目管理',
                        hideMenu: false,
                    },
                },
            ]
        },
        {
            path: '/banner',
            name: 'banner',
            component: Main,
            children: [
                {
                    path: '/banner_page',
                    name: 'banner_page',
                    component: Banner,
                    meta: {
                        icon: 'remix-icon-apps-line',
                        title: '广告管理',
                        hideMenu: false,
                    },
                },
            ]
        },
        {
            path: '/notice',
            name: 'notice',
            component: Main,
            children: [
                {
                    path: '/notice_page',
                    name: 'notice_page',
                    component: Notice,
                    meta: {
                        icon: 'remix-icon-volume-up-line',
                        title: '公告管理',
                        hideMenu: false,
                    },
                },
            ]
        },
        {
            path: '/message',
            name: 'message',
            component: Main,
            children: [
                {
                    path: '/message_page',
                    name: 'message_page',
                    component: Message,
                    meta: {
                        icon: 'ios-notifications-outline',
                        title: '消息管理',
                        hideMenu: true,
                    },
                },
            ]
        },
        {
            path: '/liuyan',
            name: 'liuyan',
            component: Main,
            children: [
                {
                    path: '/liuyan_page',
                    name: 'liuyan_page',
                    component: Liuyan,
                    meta: {
                        icon: 'remix-icon-message-3-line',
                        title: '留言管理',
                        hideMenu: false,
                    },
                },
            ]
        },
        {
            path: '/system',
            name: 'system',
            component: Main,
            meta: {
                icon: 'remix-icon-settings-4-line',
                title: '系统管理',
                hideMenu: false,
            },
            children: [
                {
                    path: '/api_history',
                    name: 'api_history',
                    component: ApiHistory,
                    meta: {
                        title: '访问记录',
                        hideMenu: false,
                    },
                },
                {
                    path: '/log',
                    name: 'log',
                    component: Log,
                    meta: {
                        title: '系统日志',
                        hideMenu: false,
                    },
                },
                {
                    path: '/resource',
                    name: 'resource',
                    component: Resource,
                    meta: {
                        title: '资源管理',
                        hideMenu: false,
                    },
                },
            ]
        },
    ]
})
