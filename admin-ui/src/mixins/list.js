export default {
    data() {
        return {
            apiUrl: '',
            search: {},
            page: {
                total: 0,
                page: 1,
                limit: 10
            },
            loading: false,
            table: {
                header: [],
                body: []
            }
        }
    },
    created() {
        this.getDataFn()
    },
    methods: {
        handlerSearch() {
            this.page.page = 1
            this.getDataFn()
        },
        async getDataFn() {
            this.loading = true
            const params = {
                ...this.search,
                page: this.page.page,
                limit: this.page.limit
            }
            try {
                const {data} = await this.$http.get(this.apiUrl, {params})
                this.table.body = data.list
                this.page.total = data.count
                this.page.page = parseInt(data.page)
            } catch (e) {
                console.error(e)
            }
            this.loading = false
        },
        pageFn(v) {
            this.page.page = v
            this.getDataFn()
        },
        pageSizeChangeFn(v) {
            this.page.limit = v
            this.getDataFn()
        },
    }
}
