import _ from 'lodash'
import {Message} from 'iview'
import dayjs from 'dayjs'

export default {
    storeGet: name => {
        const data = localStorage.getItem(name)
        try {
            return JSON.parse(data)
        } catch (e) {
            return data
        }
    },
    storeSet: (key, value) => {
        let data = value
        if (_.isObject(value)) {
            data = JSON.stringify(value)
        }
        localStorage.setItem(key, data)
    },
    storeRemove: (key) => {
        localStorage.removeItem(key)
    },
    errMessage: (e, name) => {
        if (![401, 403].includes(e.code)) {
            if (e.detail) {
                if (_.isArray(e.detail)) {
                    for (let i of e.detail) {
                        Message.error(i.message_cn)
                    }
                } else {
                    Message.error(name + '失败')
                }
            } else {
                Message.error(e.message || name + '失败')
            }
        }
    },
    dateFormat: (date = new Date(), format = 'YYYY-MM-DD') => {
        return dayjs(date).format(format)
    },
    dayjs,
    resetData: (data, {key = []}) => {
        return data
    },
    deepClone: data => {
        return _.cloneDeep(data)
    },
    baseURL: process.env.VUE_APP_SERVE
}
