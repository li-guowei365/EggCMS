import axios from "./axios";
import API from './API_PATH'
export default (file) => {
    let formData = new FormData()
    formData.append('file', file)
    return new Promise(async (resolve, reject) => {
        try {
            let {data} = await axios.post(API.FILE, formData)
            resolve(data)
        } catch (e) {
            reject(e)
        }
    })
}
