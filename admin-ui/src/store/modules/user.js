import $http from '../../lib/axios'
import router from '../../router'
import API from '../../lib/API_PATH'
import {Message} from 'iview'
import utils from '../../lib/utils'

export default {
    state: {
        info: {
            username: '',
            name: '',
            root: false,
            headImg: '',
            phone: '',
            sex: '',
            email: '',
            qq: '',
            weixin: '',
            summary: '',
            token: '',
            status: '',
            role: '',
            createAt: '',
        }
    },
    mutations: {
        setUserInfo(state, info) {
            state.info = info
        }
    },
    actions: {
        // 获取用户信息
        async getUserInfo({commit}) {
            const {data} = await $http(API.USER_INFO)
            commit('setUserInfo', data)
        },
        async quitLogin({commit}) {
            await $http.post(API.LOGIN_QUIT)
            const data = {
                username: '',
                name: '',
                root: false,
                headImg: '',
                phone: '',
                sex: '',
                email: '',
                qq: '',
                weixin: '',
                summary: '',
                token: '',
                status: '',
                role: '',
                createAt: '',
            }
            commit('setUserInfo', data)
            utils.storeRemove('token')
            router.replace('/login')
        }
    },
    getters: {
        userInfo: state => {
            return state.info
        }
    }
}
