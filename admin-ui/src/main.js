// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import iview from 'iview'
import Axios from './lib/axios'
import utils from './lib/utils'
import store from './store/index'
import API from './lib/API_PATH'
import './assets/theme.less'
import './assets/scss/index.scss'

Vue.config.productionTip = false
Vue.use(iview)
Vue.prototype.$http = Axios
Vue.prototype.$utils = utils
Vue.prototype.$API = API
/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    data: {
        FILE_PATH: process.env.VUE_APP_FILE_PATH,
    },
    render: h => h(App)
})
