module.exports = {
    lintOnSave: false,
    baseUrl: process.env.NODE_ENV === 'production'
        ? '/view/admin'
        : '/',
    css: { // 配置css模块
        loaderOptions: { // 向预处理器 Loader 传递配置选项
            less: { // 配置less（其他样式解析用法一致）
                javascriptEnabled: true // 设置为true
            }
        }
    },
    devServer: {
        proxy: {
            '/api': {
                target: 'http://127.0.0.1:7001/',
                changOrigin: true,
            }
        }
    }
}
