
let ct = document.querySelector('#copyTextarea')

document.querySelectorAll('.icon-item').forEach(e => {
    e.addEventListener('click', e => {
        const text = e.target.innerText
        ct.value = text
        ct.select()
        document.execCommand('copy')
    })
})

document.querySelector('#search').addEventListener('input', e => {
    const text = e.target.value
    let timer = null
    clearTimeout(timer)
    timer = setTimeout(() => {
        searchIcon(text)
    }, 400)
})

function searchIcon(text) {
    document.querySelectorAll('.icon-item').forEach(e => {
        const name = e.innerText
        if (name.includes(text)) {
            e.style.display = 'block'
        } else {
            e.style.display = 'none'
        }
    })
}