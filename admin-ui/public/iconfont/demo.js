const fs = require('fs');

let str = fs.readFileSync('./remixicon.css').toString()
let strArr =str.split('\n')
const domList = strArr.map(item => {
    const name = item.split(':')[0].replace(/\./ig, '').replace(/remixicon-/ig, 'icon-')
    return `<div class="icon-item">
    <div class="icon"><i class="${name}"></i></div>
    <div class="name">${name}</div>
</div>`
})

const htmlStr = `

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <div class="icon-list">
        ${domList.join('')}
    </div>
</body>

</html>

`

fs.writeFileSync('demo1.html', htmlStr)
// fs.writeFileSync('remixicon.css', str.replace(/icon-/ig, 'remix-icon-'))
